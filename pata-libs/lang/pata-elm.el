;;; pata-elm.el --- Elm mode for Emacs
;;
;; Homepage: https://github.com/jcollard/elm-mode
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package elm-mode
  :interpreter (("\\.elm\\'" . elm-mode))
)

(provide 'pata-elm)
;;; pata-elm.el ends here
