;;; pata-web.el
;;
;;
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package web-mode
  :config
  (setq web-mode-content-types-alist '(("jsx" . "\\.js[x]?\\'"))
        web-mode-markup-indent-offset 4
        web-mode-css-indent-offset 4
        web-mode-script-padding 4
        )
  :mode (("\\.html$\\'" . web-mode)
         ("\\.css$\\'" . web-mode)
         ("\\.jsx?$\\'" . web-mode)

         )
  )

(provide 'pata-web)
;;; pata-web.el ends here
