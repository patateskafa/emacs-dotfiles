;;; pata-haskell.el --- Emacs mode for Haskell
;;
;; Homepage: https://github.com/haskell/haskell-mode
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package haskell-mode
  :defer t
  :interpreter (("\\.hs\\'" . haskell-mode))
  :config
  ;; (add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
  ;; (add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
  ;; (add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
  (add-hook 'haskell-mode-hook 'flycheck-mode)
  ;; (add-hook 'haskell-mode-hook 'hindent-mode)
  (add-hook 'haskell-mode-hook 'interactive-haskell-mode)

  (use-package flycheck-haskell
    :config
    (add-hook 'flycheck-mode-hook 'flycheck-haskell-setup))

  ;; (use-package hlint-refactor
  ;;   :ensure t
  ;;   :config
  ;;   (add-hook 'haskell-mode-hook 'hlint-refactor-mode)
  ;;   :init
  ;;     (setq haskell-stylish-on-save t))

  ;; (use-package hindent
  ;;   :init
  ;;   (setq-default hindent-process-path "brittany")
  ;;   (setq-default hindent-reformat-buffer-on-save t)
  ;;   :bind
  ;;   (:map hindent-mode-map
  ;;               ("M-q" . hindent-reformat-decl-or-fill)))
  )

(provide 'pata-haskell)
;;; pata-haskell.el ends here
