;;; pata-lsp.el --- LSP bundle
;;
;; Homepage:
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;


(use-package lsp-mode
  :init
  :defer t
  :ensure t
  ;; set prefix for lsp-command-ke:post-config
  :config
  (setq lsp-keymap-prefix "C-c l"
        lsp-headerline-breadcrumb-enable-diagnostics nil
        lsp-headerline-breadcrumb-enable t
        lsp-lens-enable t
        lsp-ui-sideline-show-code-actions nil)
 ;; (define-key lsp-mode-map (kbd "C-c l") 'lsp-execute-code-action)
 ;; (add-hook 'lsp-headerline-breadcrumb-mode-hook 'flyspell-mode-off)
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
         (clojure-mode . lsp)
         (clojurec-mode . lsp)
         (clojurescript-mode . lsp)
         (elixir-mode . lsp)
         (c-mode . lsp)
         (c++-mode . lsp)
         ;; if you want which-key integration
         (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp)

;; optionally
(use-package lsp-ui
  :defer t
  :ensure t
  :config
  (setq lsp-ui-sideline-show-hover nil
        lsp-ui-doc-show-with-cursor nil
        lsp-ui-doc-show-with-mouse t
        lsp-ui-doc-delay 3
        lsp-ui-doc-position 'top
        lsp-ui-doc-alignment 'frame
        lsp-ui-doc-header nil
        lsp-ui-doc-include-signature t
        lsp-ui-doc-use-childframe t))

(use-package helm-lsp
  :defer t
  :ensure t
  :commands helm-lsp-workspace-symbol)

(use-package lsp-treemacs
  :defer t
  :ensure t)

(use-package which-key
    :config
    (which-key-mode))

(provide 'pata-lsp)
;;; pata-lsp.el ends here
