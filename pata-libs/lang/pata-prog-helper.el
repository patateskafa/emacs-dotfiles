;;; pata-prog-helper.el --- helper tools for programming mode
;;
;; ggtags- Emacs frontend to GNU Global source code tagging system
;; Homepage: https://github.com/leoliu/ggtags
;; GNU GLOBAL helm interface
;; Homepage: https://github.com/syohex/emacs-helm-gtags
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package helm-gtags
	:commands
	(helm-gtags-mode)
	:init
	(add-hook 'dired-mode-hook 'helm-gtags-mode)
	;(add-hook 'eshell-mode-hook 'helm-gtags-mode)
	(add-hook 'c-mode-hook 'helm-gtags-mode)
	(add-hook 'c++-mode-hook 'helm-gtags-mode)
	(add-hook 'asm-mode-hook 'helm-gtags-mode)
	;(add-hook 'java-mode-hook 'helm-gtags-mode)
	;(add-hook 'javascript-mode-hook 'helm-gtags-mode)
	;(add-hook 'python-mode-hook 'helm-gtags-mode)
	:bind (:map helm-gtags-mode-map
          ("C-c g a" . helm-gtags-tags-in-this-function)
          ("C-c g j" . helm-gtags-select)
          ("C-c g ." . helm-gtags-dwim)
          ("C-c g ," . helm-gtags-pop-stack)
          ("C-c g <" . helm-gtags-previous-history)
          ("C-c g >" . helm-gtags-next-history))
	:config
	(setq helm-gtags-ignore-case t
    helm-gtags-auto-uptdate t
    helm-gtags-use-input-at-cursor t
    helm-gtags-pulse-at-cursor t
    helm-gtags-prefix-key "C-c g"
    helm-gtags-suggested-key-mapping t))

(provide 'pata-prog-helper)
;;; pata-prog-helper.el ends here