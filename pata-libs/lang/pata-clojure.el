;;; pata-clojure.el --- Clojure bundle
;;
;; Homepage:
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package clojure-mode               ; Major mode for Clojure files
  :ensure t
  :defer t
  :mode (("\\.boot$" . clojure-mode)
         ("\\.clj$"  . clojure-mode)
         ("\\.cljc$"  . clojure-mode)
         ("\\.edn$"  . clojure-mode)
         ("\\.bb$" . clojure-mode))
  :hook ((clojure-mode . cider-mode)
         (clojure-mode . subword-mode)
         (clojure-mode . paredit-mode)
         (clojure-mode . hs-minor-mode))
  :config
  (evil-leader/set-key-for-mode 'clojure-mode "w)" 'sp-wrap-round)
  (evil-leader/set-key-for-mode 'clojure-mode "w]" 'sp-wrap-square)
  (evil-leader/set-key-for-mode 'clojure-mode "w}" 'sp-wrap-curly)
  (evil-leader/set-key-for-mode 'clojure-mode "wu" 'sp-unwrap-sexp)
  (evil-leader/set-key-for-mode 'clojure-mode "wks" 'sp-kill-sexp)
  (evil-leader/set-key-for-mode 'clojure-mode "wkr" 'sp-kill-region)
  (evil-leader/set-key-for-mode 'clojure-mode "wkh" 'sp-kill-hybrid-sexp)
  (evil-leader/set-key-for-mode 'clojure-mode "wkl" 'sp-kill-whole-line)
  (evil-leader/set-key-for-mode 'clojure-mode "wkw" 'sp-kill-word)
  (evil-leader/set-key-for-mode 'clojure-mode "wc" 'sp-copy-sexp)

  (evil-leader/set-key-for-mode 'clojurescript-mode "w)" 'sp-wrap-round)
  (evil-leader/set-key-for-mode 'clojurescript-mode "w]" 'sp-wrap-square)
  (evil-leader/set-key-for-mode 'clojurescript-mode "w}" 'sp-wrap-curly)
  (evil-leader/set-key-for-mode 'clojurescript-mode "wu" 'sp-unwrap-sexp)
  (evil-leader/set-key-for-mode 'clojurescript-mode "wks" 'sp-kill-sexp)
  (evil-leader/set-key-for-mode 'clojurescript-mode "wkr" 'sp-kill-region)
  (evil-leader/set-key-for-mode 'clojurescript-mode "wkh" 'sp-kill-hybrid-sexp)
  (evil-leader/set-key-for-mode 'clojurescript-mode "wkl" 'sp-kill-whole-line)
  (evil-leader/set-key-for-mode 'clojurescript-mode "wkw" 'sp-kill-word)
  (evil-leader/set-key-for-mode 'clojurescript-mode "wc" 'sp-copy-sexp)
  )

(use-package flycheck-clj-kondo
  :ensure t)

(use-package cider
  :defer t
  :ensure t
  :hook (cider-mode . eldoc-mode)
  :config (setq cider-save-file-on-load nil))

(use-package cider-stacktrace           ; Navigate stacktrace
  :ensure cider
  :defer t)

(use-package cider-util                 ; Common utilities
  :defer t
  :ensure cider)

;; (use-package clj-refactor               ; Refactoring utilities
;;   :defer t
;;   :ensure t
;;   :hook (clojure-mode . (lambda ()
;;                           (clj-refactor-mode 1)
;;                           (yas-minor-mode 1)
;;                           (cljr-add-keybindings-with-prefix "C-c RET"))))

(use-package paredit
  :defer t
  :ensure t)

(use-package aggressive-indent
  :defer t
  :ensure t
  ;; :hook ((clojure-mode . aggressive-indent-mode)
  ;;        (clojurescript-mode . aggressive-indent-mode))
  )

(provide 'pata-clojure)
;;; pata-clojure.el ends here
