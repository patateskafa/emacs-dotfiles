### Managed fully by [use-package](https://github.com/jwiegley/use-package)
A configuration from the fingers of an Elisp _n00b_, targeting mainly
the GNU/Linux environments. I wanted it to be like an intro to some of
the well known Emacs community packages for fellow newbies just like
me. Links and short descriptions of the critical packages I used are
provided below so hopefully you won't get as exhausted as I did while
losing my way countless times wandering the mazes of endless
possibilities offered by Emacs.

The short/middle-term aim of this starter config is getting used to
__The One True Editor__ -with the blasphemous __Evil__ layer- itself, to
make it extensible for future programming needs and to write my
M.Sc. thesis with the comfort that it provides. I'll
be tweaking it constantly.  Feel free to use it as a basis or a whole,
modify it, grab some snippets, criticize, make suggestions, distribute
etc.

### To back up your current setup and then install
```shell
$ mv ~/.emacs.d ~/.emacs.d.old
$ git clone --recurse-submodules https://gitlab.com/patateskafa/emacs-dotfiles.git ~/.emacs.d
$ emacs #or emacs -nw depending on the situation
```
If you're a Windows user without [Cygwin](https://www.cygwin.com),
[MinGW](http://www.mingw.org) (I don't know if it works under WSL at the
moment); the config directory is most likely to be under
C:\Users\\'%YOUR_USERNAME%'\AppData\Roaming you can back it up
yourself and copy the new one under that directory as `.emacs.d`. Though
I can't guarantee full-functionality under Windows.

And ta da! The great use-package macro will start downloading every
single package from central repos [ELPA](https://elpa.gnu.org) and
[MELPA](https://melpa.org/#/). Since `use package` also handles the
loading deferment of them, you'll probably end up with an Emacs setup
with a load time under 2 seconds. It may prompt you for the installation
of some; but remember you can always prevent any package from
downloading/loading by commenting out that particular package from
`init.el`.

### Some of the key packages and short descriptions
- [Better Defaults](https://github.com/technomancy/better-defaults) | provides some simple and useful tweaks
- [Evil](https://github.com/emacs-evil/evil) vi layer and plugins:
  - [evil-leader](https://github.com/cofi/evil-leader) | leader key implementation in evil mode
  - [evil-escape](https://github.com/syl20bnr/evil-escape) | gives great escaping capabilities! k.o.!
  - [evil-nerd-commenter](https://github.com/redguardtoo/evil-nerd-commenter) | easy comments, can be used standalone
  - [evil-snipe](https://github.com/hlissner/evil-snipe) | easy inline search
  - [evil-surround](https://github.com/emacs-evil/evil-surround) | for fast surrounds
  - [evil-visualstar](https://github.com/bling/evil-visualstar) | search for selection in visual mode
- [Magit](https://github.com/magit/magit) | a git porcelain inside emacs :)
- [Org Mode](http://orgmode.org/) | the great org mode for almost everything
  - [org-bullets](https://github.com/sabof/org-bullets) | utf-8 bullets for org-mode
  - [org-journal](https://github.com/bastibe/org-journal) | org-mode based simple journaling mode
- [Eyebrowse](https://github.com/wasamasa/eyebrowse) | manage window configs
- [Neotree](https://github.com/jaypei/emacs-neotree) | tree plugin like nerdtree for vim
- [Helm](https://github.com/emacs-helm/helm) and plugins | heavy but great completion & narrowing
    - [helm-ag](https://github.com/syohex/emacs-helm-ag) | silver searcher with helm interface
    - [helm-descbinds](https://github.com/emacs-helm/helm-descbinds) | helm frontend for describe-bindings
    - [helm-flyspell](https://github.com/pronobis/helm-flyspell) | extension for correcting words with flyspell
    - [helm-gtags](https://github.com/syohex/emacs-helm-gtags) | gnu global helm interface
    - [helm-projectile](https://github.com/bbatsov/helm-projectile) | helm ui for projectile plugin
    - [helm-swoop](https://github.com/ShingoFukuyama/helm-swoop) | moar narrowing & fast search!
    - [helm-tramp](https://github.com/masasam/emacs-helm-tramp) | tramp helm interface
    - [helm-yas-complete](https://github.com/emacs-jp/helm-c-yasnippet) | helm source for yasnippet
- [Spaceline](https://github.com/TheBB/spaceline) | powerline theme from spacemacs
- [Nyan Mode](https://github.com/TeMPOraL/nyan-mode) | pretty self-explanatory :)
- [Smartparens](https://github.com/Fuco1/smartparens) | smart parantheses!
- [Rainbow Delimiters](https://github.com/Fanael/rainbow-delimiters) | #1 weapon to fight in lispelheim realm
- [Projectile](https://github.com/bbatsov/projectile) | project interaction library
- [Yasnippet](https://github.com/joaotavora/yasnippet) | a great templating system
  - [yasnippet-snippets](https://github.com/AndreaCrotti/yasnippet-snippets) | a collection of yasnippet snippets
- [Company](https://github.com/company-mode/company-mode) | modular in-buffer completion framework
- [Flycheck](https://github.com/flycheck/flycheck) | on the fly syntax checking
- [PDF Tools](https://github.com/vedang/pdf-tools) | support library for PDF files
- [Pandoc-mode](https://joostkremers.github.io/pandoc-mode/) | convert one markup language into another, woohoo!
  - [ox-pandoc](https://github.com/kawabata/ox-pandoc) | export org mode to various formats
- [Markdown Mode](https://jblevins.org/projects/markdown-mode/) | a major mode for editing markdown-formatted text
- [Emacs-w3m](https://github.com/ecbrown/emacs-w3m) | emacs interface to w3m browser
- [All the icons](https://github.com/domtronn/all-the-icons.el) | a utility package to collect great icons
- Themes and fonts | for ze bohème to be fulfilled
  - [Gruvbox](https://github.com/Greduan/emacs-theme-gruvbox)
  - [Zenburn](https://github.com/bbatsov/zenburn-emacs)
  - [Solarized](https://github.com/bbatsov/solarized-emacs)
  - [Office Code Pro](https://github.com/nathco/Office-Code-Pro)
  - [Font Awesome](http://fontawesome.io)
  - [File Icons](https://atom.io/packages/file-icons)
  - [Weather Icons](https://erikflowers.github.io/weather-icons/)
  - [Material Icons](http://google.github.io/material-design-icons/)
  - [OctIcons](https://octicons.github.com)

### Some of the dependencies:
- [GNU Emacs](https://www.gnu.org/software/emacs/) itself :)
- [LaTeX](https://www.latex-project.org/get/)
- [The Silver Searcher (ag)](https://geoff.greer.fm/ag/)
- [Git](https://git-scm.com)
- [GNU Aspell](http://aspell.net)
- [w3m](http://w3m.sourceforge.net)
- [Pandoc](https://pandoc.org)
- Font pack for neotree etc. provided under
  [pata-libs/fonts](pata-libs/fonts). All of the fonts provided in this
  packages as resources come with either the SIL Open Font License (OFL)
  or an MIT License.

Dependency installations may vary according to your OS.
