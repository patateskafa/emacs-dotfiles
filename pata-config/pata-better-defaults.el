;;; pata-better-defaults.el --- Fixing weird quirks and poor defaults

;;; Commentary:

;;; Code:

(progn
  (unless (fboundp 'helm-mode)
    (ido-mode t)
    (setq ido-enable-flex-matching t))

  (menu-bar-mode -1)				;these
  (when (fboundp 'tool-bar-mode)	;remove
    (tool-bar-mode -1))				;unwanted
  (when (fboundp 'scroll-bar-mode)	;GUI
    (scroll-bar-mode -1))			;elements
  (when (fboundp 'horizontal-scroll-bar-mode)
    (horizontal-scroll-bar-mode -1))

  (autoload 'zap-up-to-char "misc"
    "Kill up to, but not including ARGth occurrence of CHAR." t)


(use-package saveplace
  :ensure t
  :config
  (setq save-place-file (expand-file-name "saveplace" user-emacs-directory))
  (setq-default save-place t))

  (global-set-key (kbd "M-/") 'hippie-expand)
  (global-set-key (kbd "C-x C-b") 'ibuffer)
  (global-set-key (kbd "M-z") 'zap-up-to-char)

  (global-set-key (kbd "C-s") 'isearch-forward-regexp)
  (global-set-key (kbd "C-r") 'isearch-backward-regexp)
  (global-set-key (kbd "C-M-s") 'isearch-forward)
  (global-set-key (kbd "C-M-r") 'isearch-backward)

  (show-paren-mode 1)
  (setq-default indent-tabs-mode nil)
  (setq save-interprogram-paste-before-kill t
        apropos-do-all t
        mouse-yank-at-point t
        require-final-newline t
        visible-bell t
        load-prefer-newer t
        ediff-window-setup-function 'ediff-setup-windows-plain
        save-place-file (concat user-emacs-directory "places")
        backup-by-copying t
        backup-directory-alist `(("." . ,(concat user-emacs-directory
                                                 "backups")))
        auto-save-file-name-transforms `((".*", (concat user-emacs-directory "backups/") t))))

(provide 'pata-better-defaults)
;;; pata-better-defaults.el ends here
