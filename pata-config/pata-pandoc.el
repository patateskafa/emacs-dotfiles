;;; pata-pandoc.el --- The great Pandoc and ox-pandoc plugin
;;
;; Homepage: https://pandoc.org/
;; 			 https://github.com/kawabata/ox-pandoc
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package pandoc-mode
  :defer t
  :config 
	(setq pandoc-data-dir "~/.emacs.d/pata-pandoc/")
	(add-hook 'pandoc-mode-hook 'pandoc-load-default-settings)
	(add-hook 'markdown-mode-hook 'pandoc-load-default-settings)
	(add-hook 'org-mode-hook 'pandoc-load-default-settings)
	(add-hook 'pandoc-mode-hook 'pandoc-load-default-settings)
  ;; Org integration with pandoc
  (use-package ox-pandoc
    :if (executable-find "pandoc -s")
    :config (progn
        ;; Use external css for html5
        (let ((stylesheet (concat user-emacs-directory
                                  "/pata-libs/misc/github-pandoc.css")))
          (setq org-pandoc-options-for-html5
                `((css . ,(concat "file://" stylesheet))))))))

(provide 'pata-pandoc)
;;; pata-pandoc.el ends here