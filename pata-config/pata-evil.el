;;; pata-evil.el --- The extensible vi layer for Emacs.
;;
;; Homepage: https://github.com/emacs-evil/evil
;; Bonus:    youtube.com/watch?v=5OXk4h2CBhE
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package evil
  :init
  (setq evil-search-module 'evil-search
        evil-want-C-u-scroll t
        evil-move-cursor-back t
        evil-move-beyond-eol t
        evil-want-Y-yank-to-eol t
        evil-want-fine-undo t)
  :config
  (use-package evil-nerd-commenter)
  (use-package evil-leader
    :config
    (global-evil-leader-mode)
    (evil-leader/set-leader "<SPC>")
    ;; I only keep essential navigation, edit etc.
    ;; packages' bindings here in evil config
    ;; others are in their respective configs
    (evil-leader/set-key
      "<SPC>" 'helm-M-x
      "b" 'helm-mini
      "f" 'helm-find-files
      "F" 'helm-find
      "ho" 'helm-occur
      "hb" 'helm-resume
      "hr" 'helm-regexp
      "hh" 'helm-top
      "hg" 'helm-do-ag
      "hs" 'helm-swoop
      "hp" 'helm-projectile-find-file-in-known-projects
      "ht" 'helm-tramp
      "hy" 'helm-yas-complete
      "db" 'helm-descbinds
      "es" 'evil-surround-region
      ;;"t" 'neotree-toggle
      "t" 'treemacs
      "q" 'fill-paragraph
      "k" 'kill-buffer
      "3" 'split-window-right
      "2" 'split-window-below
      "1" 'delete-other-windows
      "0" 'delete-window
      "9" 'kill-buffer-and-window
      "m" 'previous-buffer
      "n" 'next-buffer
      "o" 'other-window
      "u" 'universal-argument
      "rb" 'revert-buffer
      "di" 'dired
      "sh" 'shell-command
      "ch" 'evil-ex-nohighlight ;clears highlights
      "cy" 'company-yasnippet
      "ss" 'delete-trailing-whitespace
      ;; Nerd Commenter bindings
      "cc" 'evilnc-comment-or-uncomment-lines
      "ci" 'evilnc-comment-or-uncomment-lines
      "cl" 'evilnc-quick-comment-or-uncomment-to-the-line
      ;; "cr" 'comment-or-uncomment-region
      ;; "cp" 'evilnc-comment-or-uncomment-paragraphs
      )
    )
  (define-key evil-normal-state-map (kbd "C-9") 'evil-end-of-line) ; $ is hard to reach with TR keyboard
  (define-key evil-normal-state-map (kbd "C-0") 'evil-digit-argument-or-evil-beginning-of-line) ; just in case
  (define-key evil-normal-state-map (kbd "C-c d") 'flyspell-auto-correct-previous-word)
  (define-key evil-normal-state-map (kbd "C-c f") 'flyspell-auto-correct-word)
  (define-key evil-insert-state-map (kbd "C-c d") 'flyspell-auto-correct-previous-word)
  (define-key evil-insert-state-map (kbd "C-c f") 'flyspell-auto-correct-word)
  (define-key evil-normal-state-map (kbd "C-c s") 'flyspell-correct-word-before-point)
  (define-key evil-insert-state-map (kbd "C-c s") 'flyspell-correct-word-before-point)
  (define-key evil-motion-state-map (kbd "C-n") 'help-go-forward)
  (define-key evil-motion-state-map (kbd "C-p") 'help-go-back)
  (use-package evil-visualstar
    :config
    (global-evil-visualstar-mode 1))
  (use-package evil-surround
    :config
    (global-evil-surround-mode 1))
  ;; problematic auto-loading for now
  ;; check pata-general.el for helper func
  (use-package evil-snipe
    :config
    (setq evil-snipe-scope 'whole-buffer
          evil-snipe-enable-highlight t
          evil-snipe-enable-incremental-highlight t
          evil-snipe-auto-disable-substitute t
          evil-snipe-show-prompt nil
          evil-snipe-smart-case t))
  (use-package evil-escape
    :config
    (evil-escape-mode)
    (setq-default evil-escape-key-sequence "kq" ; K.O.!
                  evil-escape-delay 0.5))
  (evil-mode 1)
  ;; command aliases
  (evil-ex-define-cmd "W" "w")
  (evil-ex-define-cmd "Wq" "wq")
  (evil-ex-define-cmd "WQ" "wq")
  (evil-ex-define-cmd "Q" "q")
  (setq evil-insert-state-cursor '("#AFAF00" bar)
        evil-normal-state-cursor '("#5F8787" box)
        evil-visual-state-cursor '("#AFAF87" box)
        evil-motion-state-cursor '("#D3869B" box)
        evil-replace-state-cursor '("#AFAF87" box)
        evil-emacs-state-cursor `("#F4E8BA" box))
  ;; below enables C-left and C-right movements in ansi-term
  (lambda ()
        (defun term-send-evil-Cright () (interactive) (term-send-raw-string "\e[1;5C"))
        (defun term-send-evil-Cleft  () (interactive) (term-send-raw-string "\e[1;5D")))
  (evil-define-key 'insert term-mode-map
    (kbd "C-<right>") 'term-send-evil-Cright
    (kbd "C-<left>") 'term-send-evil-Cleft)
  (evil-define-key 'insert term-raw-map
    (kbd "<delete>") 'term-send-del
    (kbd "C-w") 'term-send-raw))

(use-package undo-tree
  :ensure t
  :after evil
  :diminish
  :config
  (evil-set-undo-system 'undo-tree)
  (global-undo-tree-mode 1))

(provide 'pata-evil)
;;; pata-evil.el ends here
