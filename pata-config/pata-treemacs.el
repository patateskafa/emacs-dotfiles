;;; pata-treemacs.el
;;
;; Homepage:
;;
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package treemacs
  :defer t
  :ensure t
  :commands (treemacs
             treemacs-follow-mode
             treemacs-filewatch-mode
             treemacs-toggle
             treemacs-bookmark
             treemacs-find-file
             treemacs-select-window)
  :config
  (progn
    (setq treemacs-max-git-entries 500
          treemacs-width 50
          treemacs-indentation 1
          treemacs-width-increment 1)

    (treemacs-indent-guide-mode t)
    (treemacs-follow-mode t)
    (treemacs-git-mode 'deferred)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode 'always)))

(use-package treemacs-projectile
  :ensure t
  :after treemacs projectile)

(use-package treemacs-magit
  :ensure t
  :after treemacs magit)

(use-package treemacs-icons-dired
  :ensure t
  :after treemacs dired)

(use-package treemacs-all-the-icons
  :ensure t
  :after treemacs all-the-icons)

(provide 'pata-treemacs)
;;; pata-treemacs.el ends here
