;;; pata-smartparens.el --- Minor mode for Emacs that deals with parens pairs and tries to be smart about it.
;;
;; Homepage: https://github.com/Fuco1/smartparens
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package smartparens
  :config
  (progn
      ;; press RET, the curly braces automatically
    ;; add another newline
    (sp-with-modes '(c-mode c++-mode java-mode)
                   (sp-local-pair "{" nil :post-handlers '(("||\n[i]" "RET")))
                   (sp-local-pair "/*" "*/" :post-handlers '((" | " "SPC")
                                                             ("* ||\n[i]" "RET"))))
    (setq sp-highlight-pair-overlay nil)
    (add-hook 'prog-mode-hook 'smartparens-mode)
    (add-hook 'prog-mode-hook 'show-smartparens-mode)))

(provide 'pata-smartparens)
;;; pata-smartparens.el ends here
