;;; pata-projectile.el --- Project Interaction Library for Emacs and Helm UI for Projectile
;;
;; Homepage: https://github.com/bbatsov/projectile
;; 			 https://github.com/bbatsov/helm-projectile
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package projectile
  :config
  (progn
    (projectile-mode 1)
    (global-set-key (kbd "C-c hp") 'helm-projectile)))

(use-package helm-projectile
  :config
  (helm-projectile-on))

(provide 'pata-projectile)
;;; pata-projectile.el ends here