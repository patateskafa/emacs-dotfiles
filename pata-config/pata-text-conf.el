;;; pata-text-conf.el --- finer grained control for ze texts
;;
;;
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package rainbow-mode
  :defer t
  :init
  (add-hook 'text-mode-hook 'turn-on-auto-fill)
  (add-hook 'text-mode-hook
  	'(lambda() (set-fill-column 72)))
  (add-hook 'text-mode-hook 'turn-on-flyspell)
  (add-hook 'text-mode-hook 'turn-on-visual-line-mode)
  (add-hook 'css-mode-hook 'rainbow-mode)
  (add-hook 'scss-mode-hook 'rainbow-mode)

  ;;(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode) ; maybe one day
  (add-hook 'LaTeX-mode-hook 'turn-on-flyspell)
  (add-hook 'LaTeX-mode-hook 'TeX-PDF-mode)
  (add-hook 'LaTeX-mode-hook 'turn-on-auto-fill)
  (add-hook 'LaTeX-mode-hook
  	'(lambda() (turn-on-reftex)))
  ;;(add-hook 'LaTeX-mode-hook 'Tex-source-correlate-mode)

  (add-hook 'markdown-mode-hook 'turn-on-flyspell)
  (add-hook 'markdown-mode-hook 'turn-on-auto-fill)

  ;;(add-hook 'prog-mode-hook 'flyspell-prog-mode) ; maybe later
  (add-hook 'prog-mode-hook
  	'(lambda() (toggle-trailing-whitespace-display))))

(provide 'pata-text-conf)
;;; pata-text-conf.el ends here
