;;; pata-solarized.el --- The Solarized colour theme, ported to Emacs
;;
;; Homepage: https://github.com/bbatsov/solarized-emacs/
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package solarized-theme
    :init
    (progn
        (setq solarized-use-less-bold t
            solarized-distinct-fringe-background nil
            solarized-emphasize-indicators nil
            solarized-height-minus-1 1.0
            solarized-height-plus-1 1.0
            solarized-height-plus-2 1.0
            solarized-height-plus-3 1.0
            solarized-height-plus-4 1.0
            solarized-high-contrast-mode-line t
            solarized-scale-org-headlines nil
            solarized-use-more-italic t
            solarized-use-variable-pitch t))
    :config
    (progn
        (load "solarized-theme-autoloads" nil t)
        (setq theme-dark 'solarized-dark
            theme-bright 'solarized-light))
    (load-theme 'solarized-light t))

(provide 'pata-solarized)
;;; pata-solarized.el ends here