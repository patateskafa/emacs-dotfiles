;;; pata-eshell.el --- Eshell is a shell-like command interpreter implemented in Emacs Lisp.
;;
;; Homepage: https://www.gnu.org/software/emacs/manual/html_mono/eshell.html
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package eshell
  :defer t
  :config
  (progn
    (setq eshell-prompt-function
          (lambda ()
            (concat "[" (getenv
                         (cond
                          ((string-match "i686-pc-mingw32" system-configuration) "USERNAME")
                          ((string-match "x86_64-w64-mingw32" system-configuration) "USERNAME")
                          ((string-match "cygwin" system-configuration) "USER")
                          ((string-match "linux" system-configuration) "USER")))
                    "@" (car (split-string (system-name) "\\.")) " "
                    (car (last (split-string (eshell/pwd) "/")))
                    "] " (if (= (user-uid) 0) "# " "$ ")))
          eshell-directory-name (expand-file-name "eshell" user-emacs-directory))))

(provide 'pata-eshell)
;;; pata-eshell.el ends here