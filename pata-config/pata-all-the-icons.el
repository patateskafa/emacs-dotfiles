;;; pata-all-the-icons.el --- A utility package to collect various Icon Fonts and propertize them within Emacs.
;;
;; Homepage: https://github.com/domtronn/all-the-icons.el
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package all-the-icons)

(provide 'pata-all-the-icons)
;;; pata-all-the-icons.el ends here