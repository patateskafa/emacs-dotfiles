;;; pata-gruvbox.el --- Gruvbox is a retro groove color scheme for Emacs.
;;
;; Homepage: https://github.com/greduan/emacs-theme-gruvbox/
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package gruvbox-theme
  :config
  ;; options are
  ;; gruvbox-dark-soft
  ;; gruvbox-dark-medium
  ;; gruvbox-dark-hard
  ;; gruvbox-light-soft
  ;; gruvbox-light-medium
  ;; gruvbox-light-hard
  (load-theme 'gruvbox-dark-medium t))

(provide 'pata-gruvbox)
;;; pata-gruvbox.el ends here