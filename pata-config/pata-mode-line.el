;;; pata-mode-line.el ---  mode line configuration
;;
;; Powerline theme from Spacemacs & ze Nyan Mode!
;; Homepage: https://github.com/TheBB/spaceline
;;           https://github.com/TeMPOraL/nyan-mode
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package spaceline
  :config
  (require 'spaceline-config)
  ;; alternate, wave, arrow, slant, bar, utf-8, zigzag, butt etc...
  (setq powerline-default-separator 'wave
  	spaceline-workspace-numbers-unicode t
  	spaceline-window-numbers-unicode t
  	spaceline-highlight-face-func 'spaceline-highlight-face-evil-state)
  (spaceline-toggle-minor-modes-off)
  ;;(spaceline-toggle-hud-off)
  ;;(spaceline-toggle-buffer-size-off)
  (spaceline-spacemacs-theme)
  (set-face-attribute 'spaceline-evil-emacs nil :background "#F4E8BA")
  (set-face-attribute 'spaceline-evil-insert nil :background "#AFAF00")
  (set-face-attribute 'spaceline-evil-motion nil :background "#D3869B")
  (set-face-attribute 'spaceline-evil-normal nil :background "#5F8787")
  (set-face-attribute 'spaceline-evil-replace nil :background "#AFAF87)")
  (set-face-attribute 'spaceline-evil-visual nil :background "#AFAF87")
  (spaceline-helm-mode 1))

;; the package of all packages
(use-package nyan-mode
  :defer t
  :config
  (progn
    (setq nyan-animate-nyancat nil
    nyan-bar-length 8
    nyan-wavy-trail nil)))

(provide 'pata-mode-line)
;;; pata-mode-line.el ends here