;;; pata-w3m.el --- w3m Browser
;;
;; Homepage: http://emacs-w3m.namazu.org/
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package w3m
  :commands w3m
  :config
  (setq  w3m-use-cookies t
         w3m-pop-up-windows t
         w3m-use-tab nil ; no tabs for now
         w3m-user-agent (concat "Mozilla/5.0 (Linux; U; Android 2.3.3; zh-tw; "
                               "HTC_Pyramid Build/GRI40) "
                               "AppleWebKit/533.1 (KHTML, like Gecko) "
                               "Version/4.0 Mobile Safari/533."))

  (add-hook 'w3m-mode-hook (lambda ()
                             ;; start browsing in chaotic evil mode
                             (evil-set-initial-state 'w3m-mode 'normal)
                             (evil-define-key 'normal w3m-mode-map
                                (kbd "RET") 'w3m-view-this-url
                                (kbd "U") 'w3m-goto-url
                                (kbd "G") 'w3m-goto-url-new-session
                                (kbd "H") 'w3m-gohome
                                (kbd "f") 'w3m-next-form
                                (kbd "F") 'w3m-previous-form
                                (kbd "S") 'w3m-submit-form
                                (kbd "p") 'w3m-view-previous-page
                                (kbd "n") 'w3m-view-next-page
                                (kbd "r") 'w3m-reload-this-page
                                (kbd "R") 'w3m-redisplay-this-page
                                (kbd "i") 'w3m-next-image
                                (kbd "I") 'w3m-previous-image
                                (kbd "x") 'w3m-save-image
                                (kbd "d") 'w3m-download
                                (kbd "\\") 'w3m-view-source
                                (kbd "=") 'w3m-view-header
                                (kbd "c") 'w3m-cookie
                                (kbd "s") 'w3m-history
                                (kbd "a") 'w3m-bookmark-add-current-url
                                (kbd "C-a") 'w3m-bookmark-add-this-url
                                (kbd "v") 'w3m-bookmark-view
                                (kbd "q") 'w3m-close-window
                                (kbd "Q") 'w3m-quit
                                (kbd "C-u") (lambda ()
                                                                  (interactive)
                                                                  (w3m-scroll-down
                                                                   (/ (window-height) 2)))
                                (kbd "C-d") (lambda ()
                                                                  (interactive)
                                                                  (w3m-scroll-up
                                                                   (/ (window-height) 2)))))))

(provide 'pata-w3m)
;;; pata-w3m.el ends here