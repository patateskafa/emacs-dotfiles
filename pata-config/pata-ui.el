;;; pata-ui.el --- General UI settings
;;
;; Homepage: https://github.com/nathco/Office-Code-Pro
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(cond ((eq (window-system) 'x)
		;(add-to-list 'default-frame-alist '(fullscreen . maximized))
   (add-to-list 'default-frame-alist '(font . "Office Code Pro 10"))

;(add-to-list 'default-frame-alist '(font .  "Office Code Pro 16"))
;(set-face-attribute 'default t :font  "Office Code Pro 16")



       
       ))

(provide 'pata-ui)
;;; pata-ui.el ends here
