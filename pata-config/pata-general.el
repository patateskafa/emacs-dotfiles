;;; pata-general.el --- general configuration
;;
;;
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

;; utf-8 domination!
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
;; Allow 20MB of memory (default -> 0.76MB )
;; before GC. This means GC runs less often.
(setq gc-cons-threshold (* 20 1024 1024))
;; auto revert buffer when file changes on disk
(global-auto-revert-mode t)
;; remove trailing whitespaces before save
;; (add-hook 'before-save-hook 'delete-trailing-whitespace)
;; enable to support navigate in camelCase words
(global-subword-mode t)
;; fix incremental search making screen look funny in terminal
(add-hook 'isearch-update-post-hook 'redraw-display)
;;sacrifice "yes or no" prompt to the gods of brevity
(fset 'yes-or-no-p 'y-or-n-p)
;; echoes pressed keys in the echo area
(setq echo-keystrokes 0.1)
;; shut ring bell up
(setq ring-bell-function 'ignore)
;; enable S + arrow key switch between windows
;; (windmove-default-keybindings)
;; change custom variables file
;; location anr read from it
(setq custom-file "~/.emacs.d/custom.el")
(if (file-exists-p custom-file)
	(load custom-file))
;; jump mouse away when approached
;;(mouse-avoidance-mode 'jump)
;; switches to help window to quit with q
(setq help-window-select t)
;; turn on global highlight line mode except terminals
(if (display-graphic-p) (global-hl-line-mode t))
;; display line numbers in below modes
(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(add-hook 'text-mode-hook 'display-line-numbers-mode)
(add-to-list 'auto-mode-alist '("\\.bib\\'" . display-line-numbers-mode))
;; enlarge current window proportionally
(defun enlarge-window-proportionally ()
  (interactive)
  (enlarge-window 3)
  (enlarge-window-horizontally 3))
;; shrink current window proportionally
(defun shrink-window-proportionally ()
  (interactive)
  (shrink-window 3)
  (shrink-window-horizontally 3))
;; enlarge/shrink window keybindings
(global-set-key (kbd "<S-f8>") 'shrink-window-proportionally)
(global-set-key (kbd "<f8>") 'enlarge-window-proportionally)
(global-set-key (kbd "<f9>") 'enlarge-window-horizontally)
(global-set-key (kbd "<f10>") 'enlarge-window)
(global-set-key (kbd "<S-f9>") 'shrink-window-horizontally)
(global-set-key (kbd "<S-f10>") 'shrink-window)
(global-set-key (kbd "<ESC> <ESC> <ESC>") 'keyboard-quit)
;; helper function for the non-loading snipe mode
(defun evil-snipe-local-mode-toggle ()
  "Toggle evil-snipe on/off"
  (interactive)
  (evil-snipe-local-mode)
  (evil-snipe-override-local-mode)
  (if (get 'evil-snipe-local-mode-toggle 'state)
      (progn
        (evil-snipe-local-mode -1)
        (evil-snipe-override-local-mode -1)
        (message "evil-snipe off!")
        (put 'evil-snipe-local-mode-toggle 'state nil))
    (progn
      (evil-snipe-local-mode +1)
      (evil-snipe-override-local-mode +1)
      (message "evil-snipe on!")
      (put 'evil-snipe-local-mode-toggle 'state t))))
;; snipe mode local toggle keybinding
(global-set-key (kbd "<f7>") 'evil-snipe-local-mode-toggle)
;; toggle show trailing whitepace on/off
(defun toggle-trailing-whitespace-display ()
  (interactive)
  (save-excursion
    (if show-trailing-whitespace
        (setq show-trailing-whitespace nil)
      (setq show-trailing-whitespace t))
    (force-window-update (current-buffer)))
  (message (concat
            "Display of EOL spaces "
            (if show-trailing-whitespace "enabled" "disabled"))))
;; toggle show trailing whitepace keybinding
(global-set-key (kbd "<S-f7>") 'toggle-trailing-whitespace-display)
;; turn it off by default
(setq-default show-trailing-whitespace nil)
;; below enables C-left and C-right movements in ansi-term
(add-hook 'term-load-hook
  (lambda ()
      (defun term-send-Cright () (interactive) (term-send-raw-string "\e[1;5C"))
      (defun term-send-Cleft  () (interactive) (term-send-raw-string "\e[1;5D"))
      (define-key term-raw-map (kbd "C-<right>") 'term-send-Cright)
      (define-key term-raw-map (kbd "C-<left>")'term-send-Cleft)))
;; startup screen disable
(setq inhibit-startup-screen t)
;; initial message
(defun file-contents (filename)
  "Return the contents of FILENAME."
  (with-temp-buffer
    (insert-file-contents filename)
    (buffer-string)))

(setq initial-scratch-message
  (file-contents
    (expand-file-name "pata-libs/misc/splash" user-emacs-directory)))

(cond
 ((string-equal system-type "windows-nt") ; Microsoft Windows
  (progn
    (message "Microsoft Windows")))
 ((string-equal system-type "darwin") ; Mac OS X
  (progn
    (message "Mac OS X")
    (setq ns-use-native-fullscreen nil)
    (setq ns-use-fullscreen-animation nil)
    (run-at-time "5sec" nil
                    (lambda ()
                        (let ((fullscreen (frame-parameter (selected-frame) 'fullscreen)))
                        ;; If emacs has in fullscreen status, maximized window first, drag from Mac's single space.
                        (when (memq fullscreen '(fullscreen fullboth))
                            (set-frame-parameter (selected-frame) 'fullscreen 'maximized))
                        (add-to-list 'exec-path "/usr/local/bin/")
                        ;; Manipulating a frame without waiting for the fullscreen
                        ;; animation to complete can cause a crash, or other unexpected
                        ;; behavior, on macOS (bug#28496).
                        (sleep-for 0.5)
                        ;; Call `toggle-frame-fullscreen' to fullscreen emacs.
                        (toggle-frame-fullscreen))))
        (setq mac-right-option-modifier 'none)
        ))
 ((string-equal system-type "gnu/linux") ; linux
  (progn
    (message "Linux"))))


(provide 'pata-general)
;;; pata-general.el ends here
