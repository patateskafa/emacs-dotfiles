;;; pata-rainbow-delim.el --- Emacs rainbow delimiters mode
;;
;; Homepage: https://github.com/Fanael/rainbow-delimiters
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package rainbow-delimiters
  :config
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

(provide 'pata-rainbow-delim)
;;; pata-rainbow-delim.el ends here