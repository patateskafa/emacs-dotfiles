;;; pata-company.el --- Modular in-buffer completion framework for Emacs
;;
;; Homepage: https://github.com/company-mode/company-mode
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package company
  :config
  (add-hook 'prog-mode-hook 'company-mode)
  (add-hook 'text-mode-hook 'company-mode)
  ;; (add-hook 'org-mode-hook 'company-mode)
  (add-hook 'markdown-mode-hook 'company-mode)
  (add-hook 'LaTeX-mode-hook
              '(lambda() (company-auctex-init)))
  ;;(add-hook 'haskell-mode-hook 'company-mode)
  ;;(add-hook 'after-init-hook 'global-company-mode)
  ;;(add-hook 'company-mode-hook 'company-quickhelp-mode)
  ;;(delete 'company-dabbrev company-backends)
  (setq company-idle-delay 0.1
    company-echo-delay 0.1
    company-quickhelp-idle-delay 0.2
    company-show-numbers nil
    company-minimum-prefix-length 1
    company-quickhelp-max-lines 15)
  (add-to-list 'company-backends 'company-capf)
  ;; (use-package company-ghc
  ;;   :defer t
  ;;   :config
  ;;   (add-to-list 'company-backends
  ;;                '(company-ghc :with company-dabbrev-code))
  ;;   (custom-set-variables '(company-ghc-show-info t)))
  ;; (use-package company-auctex
  ;;   :defer t)
  )

(provide 'pata-company)
;;; pata-company.el ends here
