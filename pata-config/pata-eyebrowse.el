;;; pata-eyebrowse.el --- A simple-minded way of managing window configs in emacs
;;
;; Homepage: https://github.com/wasamasa/eyebrowse
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package eyebrowse
  :diminish eyebrowse-mode
  :config (progn
            (define-key eyebrowse-mode-map (kbd "C-q 1") 'eyebrowse-switch-to-window-config-1)
            (define-key eyebrowse-mode-map (kbd "C-q 2") 'eyebrowse-switch-to-window-config-2)
            (define-key eyebrowse-mode-map (kbd "C-q 3") 'eyebrowse-switch-to-window-config-3)
            (define-key eyebrowse-mode-map (kbd "C-q 4") 'eyebrowse-switch-to-window-config-4)
            (define-key eyebrowse-mode-map (kbd "C-q 5") 'eyebrowse-switch-to-window-config-5)
            (define-key eyebrowse-mode-map (kbd "C-q 6") 'eyebrowse-switch-to-window-config-6)
            (define-key eyebrowse-mode-map (kbd "C-q 7") 'eyebrowse-switch-to-window-config-7)
            (define-key eyebrowse-mode-map (kbd "C-q 8") 'eyebrowse-switch-to-window-config-8)
            (define-key eyebrowse-mode-map (kbd "C-q 9") 'eyebrowse-switch-to-window-config-9)
            (define-key eyebrowse-mode-map (kbd "C-q n") 'eyebrowse-next-window-config)
            (define-key eyebrowse-mode-map (kbd "C-q p") 'eyebrowse-prev-window-config)
            (define-key eyebrowse-mode-map (kbd "C-q c") 'eyebrowse-create-window-config)
            (define-key eyebrowse-mode-map (kbd "C-q w") 'eyebrowse-close-window-config)
            (eyebrowse-mode t)
            (setq eyebrowse-new-workspace t
            		eyebrowse-wrap-around t)))

(provide 'pata-eyebrowse)
;;; pata-eyebrowse.el ends here