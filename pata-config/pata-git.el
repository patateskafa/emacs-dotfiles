;;; pata-git.el --- Git utilities
;;
;; Homepage:
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package magit
  :ensure
  :bind ("C-x G" . magit-status)
  :init
  (progn
    (evil-leader/set-key
      "G" 'magit-status)
    (evil-set-initial-state 'magit-mode 'emacs)
	(evil-set-initial-state 'magit-status-mode 'emacs)
	(evil-set-initial-state 'magit-log-edit-mode 'emacs)
	(evil-set-initial-state 'git-commit-mode 'emacs)
	(evil-set-initial-state 'magit-commit-mode 'emacs)
	(evil-set-initial-state 'magit-popup-mode 'emacs)
	(evil-set-initial-state 'magit-blame-mode 'emacs)
	(evil-define-key 'motion magit-commit-mode-map
	  "\C-c\C-b" 'magit-show-commit-backward
	  "\C-c\C-f" 'magit-show-commit-forward)
	(evil-set-initial-state 'magit-diff-mode 'emacs)
	(evil-set-initial-state 'magit-wassup-mode 'emacs)
	(evil-set-initial-state 'magit-log-mode 'emacs)))

(use-package git-link
  :defer t
  :config
  ;; This might create a reference in the wrong lines if you're using branches
  ;; for development and your current version is too far ahead from origin.
  (setq git-link-default-branch "master"))

(use-package blamer
  :ensure t
  :defer t
  :custom
  (blamer-idle-time 0.3)
  (blamer-min-offset 70)
  :custom-face
  (blamer-face ((t :foreground "#fe8019"
                    :background nil
                    :height 80
                    :italic nil))))

(use-package diff-hl
  :hook (prog-mode          . diff-hl-mode)
  :hook (text-mode          . diff-hl-mode)
  :hook (dired-mode         . diff-hl-mode)
  :hook (dired-mode         . diff-hl-dired-mode)
  :hook (magit-post-refresh . diff-hl-magit-post-refresh)
  :hook (magit-pre-refresh  . diff-hl-magit-pre-refresh)
  :config
  ;; When not in a GUI use `diff-hl-margin-mode'
  (when
   (display-graphic-p)
    (add-hook 'diff-hl-mode-hook #'diff-hl-margin-mode)))

(provide 'pata-git)
;;; pata-git.el ends here
