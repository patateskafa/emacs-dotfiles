;;; pata-editorconfig.el --- EditorConfig helps maintain consistent
;;; coding styles for multiple developers working on the same project
;;; across various editors and IDEs.
;;
;; Homepage: https://editorconfig.org/
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package editorconfig
  :ensure t
  :config
  (editorconfig-mode 1))

(provide 'pata-editorconfig)
;;; pata-editorconfig.el ends here
