;;; pata-yaml.el ---The emacs major mode for editing files in the YAML data serialization format.
;;
;; Homepage: https://github.com/yoshiki/yaml-mode
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package yaml-mode
  :mode (("\\.yml$"  . yaml-mode)
         ("\\.yaml$" . yaml-mode)))

(provide 'pata-yaml)
;;; pata-yaml.el ends here
