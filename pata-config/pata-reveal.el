;;; pata-reveal.el ---  Org-Reveal exports your Org documents to reveal.js presentations.
;;
;; Homepage: https://github.com/yjwen/org-reveal
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package ox-reveal
  :after org
  :config
  (setq org-reveal-root "file:///data/devtools/revealjs/reveal.js/")
  (setq org-reveal-mathjax t))

(use-package htmlize
  :after org)

(provide 'pata-reveal)
;;; pata-reveal.el ends here
