;;; pata-helm.el --- Emacs incremental completion and selection narrowing framework
;;
;; Homepage: http://tuhdo.github.io/helm-intro.html
;;           https://github.com/emacs-helm/helm
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package helm
  :diminish helm-mode
  :init
  :bind (("M-x" . helm-M-x)
        ("C-c h x" . helm-M-x)
        ("M-y" . helm-show-kill-ring)
        ("C-x C-f" . helm-find-files)
        ("C-c h F" . helm-find)
        ("C-x b" . helm-mini)
        ("C-c h o" . helm-occur)
        ("C-c h b" . helm-resume)
        ("C-c h r" . helm-regexp)
        ("C-c h t" . helm-top)
        ("C-c b" . helm-mini)
        ("C-c h f" . helm-find-files)
        ("C-c C-c" . helm-keyboard-quit))
  :config
  (global-set-key (kbd "C-c h") 'helm-command-prefix)
  (global-unset-key (kbd "C-x c"))
  (setq helm-M-x-fuzzy-match t
    helm-buffers-fuzzy-matching t
    helm-recentf-fuzzy-match t
    helm-lisp-fuzzy-completion t
    helm-imenu-fuzzy-match    t
    helm-semantic-fuzzy-match t
    helm-apropos-fuzzy-match t
    helm-split-window-in-side-p t ; open helm buffer inside current window, not occupy whole other window
    helm-move-to-line-cycle-in-source t ; move to end or beginning of source when reaching top or bottom of source.
    helm-ff-search-library-in-sexp t ; search for library in `require' and `declare-function' sexp.
    helm-scroll-amount 8 ; scroll 8 lines other window using M-<next>/M-<prior>
    helm-ff-file-name-history-use-recentf t
    helm-echo-input-in-header-line t
    helm-autoresize-max-height 0
    helm-autoresize-min-height 30)

  (add-to-list 'helm-sources-using-default-as-input 'helm-source-man-pages)
  ;; helm-map key bindings
  ;; looks ugly now, will improve later
  (define-key helm-map (kbd "M-<up>")  'previous-history-element)
  (define-key helm-map (kbd "M-<down>")  'next-history-element)
  (define-key helm-map (kbd "C-h") 'delete-backward-char)
  (define-key helm-map (kbd "C-w") 'backward-kill-word)
  ;; (define-key helm-map (kbd "C-b") 'helm-descbinds)
  (define-key helm-map (kbd "C-j") 'helm-next-line)
  (define-key helm-map (kbd "C-k") 'helm-previous-line)
  (define-key helm-map (kbd "C-a") 'helm-toggle-visible-mark)
  (define-key helm-map (kbd "C-s") 'helm-confirm-and-exit-minibuffer)
  (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
  (define-key helm-map (kbd "C-n") 'helm-next-source)
  (define-key helm-map (kbd "C-p") 'helm-previous-source)
  (define-key helm-map (kbd "M-n") 'helm-next-page)
  (define-key helm-map (kbd "M-p") 'helm-previous-page)
  (define-key helm-map (kbd "C-d") 'helm-delete-minibuffer-contents)
  (define-key helm-map (kbd "C-y") 'helm-yank-text-at-point)
  (define-key helm-map (kbd "M-u") 'helm-unmark-all)
  (define-key helm-map (kbd "M-a") 'helm-mark-all)
  (define-key helm-map (kbd "C-c b") 'helm-find-files-backup)
  (helm-autoresize-mode t))

(use-package helm-config
  :ensure helm
  :defer t)

(use-package helm-buffers
  :ensure helm
  :defer t
  :config
    (define-key helm-buffer-map (kbd "M-k") 'helm-buffer-run-kill-buffers))

(use-package helm-files
  :ensure helm
  :defer t
  :config
  (define-key helm-find-files-map (kbd "C-c C-x") 'helm-ff-run-open-file-externally)
  (define-key helm-find-files-map (kbd "C-c C-p") 'helm-ff-browse-project)
  (define-key helm-find-files-map (kbd "C-c o") 'helm-ff-run-switch-other-window)
  (define-key helm-find-files-map (kbd "C-c C-o") 'helm-ff-run-switch-other-frame)
  (define-key helm-find-files-map (kbd "M-i") 'helm-ff-properties-persistent)
  (define-key helm-find-files-map (kbd "C-c c") 'helm-ff-run-copy-file)
  (define-key helm-find-files-map (kbd "C-c r") 'helm-ff-run-rename-file)
  (define-key helm-find-files-map (kbd "C-c d") 'helm-ff-persistent-delete))

(use-package helm-ag
  :config
  (setq helm-ag-use-grep-ignore-list t
        helm-ag-insert-at-point 'symbol))

;; (use-package helm-descbinds
;;   :config (helm-descbinds-mode))

(use-package helm-swoop)

(use-package helm-flyspell
  :config
    (define-key flyspell-mode-map (kbd "C-,") 'helm-flyspell-correct)
    (define-key flyspell-mode-map (kbd "C-;") 'flyspell-goto-next-error))

(use-package helm-org
  :after (helm org)
  :bind (("C-c s o c" . helm-org-capture-templates)
         ("C-c s o h" . helm-org-in-buffer-headings)
         ("C-c o o"   . helm-org-in-buffer-headings)))

(use-package helm-org-rifle
  :after (helm org)
  :bind  ("C-c s o r" . helm-org-rifle))

(provide 'pata-helm)
;;; pata-helm.el ends here
