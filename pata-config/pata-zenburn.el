;;; pata-zenburn.el --- Direct port of the Zenburn theme for vim
;;
;; Homepage: https://github.com/bbatsov/zenburn-emacs
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package zenburn-theme
  :config
  (progn
    (load-theme 'zenburn 1)))

(provide 'pata-zenburn)
;;; pata-zenburn.el ends here