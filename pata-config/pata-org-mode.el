;;; pata-org-mode.el --- A utility package to collect various Icon
;;; Fonts and propertize them within Emacs.
;;
;; Homepage: http://orgmode.org/
;;           https://github.com/sabof/org-bullets
;;           https://github.com/bastibe/org-journal
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package org
  :defer t
  :pin gnu
  :config

  (setq org-startup-with-inline-images t)
  (setq org-latex-create-formula-image-program 'imagemagick)  ; or 'dvipng

  (setq org-agenda-files '("~/fikir/ajanda"))

  (setq org-log-done 'time)

  (setq org-log-into-drawer t)

  (setq org-agenda-start-day "-Mon")

  (setq org-agenda-span 30)

  (setq org-todo-keywords
        '(
          (sequence "YAP(y)" "BELKİ(b)" "DEVAM(d)" "|" "SONLAN(s!)" "VAZGEÇ(v@)")
          (sequence "BACKLOG(b)" "PLAN(p)" "READY(r)" "ACTIVE(a)" "REVIEW(v)" "WAIT(w@/!)" "HOLD(h)" "|" "COMPLETED(c)" "CANC(k@)")
          ))

  ;; Configure custom agenda views
  (setq org-agenda-custom-commands
   '(("d" "Dashboard"
     ((agenda "" ((org-deadline-warning-days 7)))
      (todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))
      (tags-todo "agenda/ACTIVE" ((org-agenda-overriding-header "Active Projects")))))

    ("n" "Next Tasks"
     ((todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))))

    ("W" "Work Tasks" tags-todo "+work-email")

    ;; Low-effort next actions
    ("e" tags-todo "+TODO=\"NEXT\"+Effort<15&+Effort>0"
     ((org-agenda-overriding-header "Low Effort Tasks")
      (org-agenda-max-todos 20)
      (org-agenda-files org-agenda-files)))

    ("w" "Workflow Status"
     ((todo "WAIT"
            ((org-agenda-overriding-header "Waiting on External")
             (org-agenda-files org-agenda-files)))
      (todo "REVIEW"
            ((org-agenda-overriding-header "In Review")
             (org-agenda-files org-agenda-files)))
      (todo "PLAN"
            ((org-agenda-overriding-header "In Planning")
             (org-agenda-todo-list-sublevels nil)
             (org-agenda-files org-agenda-files)))
      (todo "BACKLOG"
            ((org-agenda-overriding-header "Project Backlog")
             (org-agenda-todo-list-sublevels nil)
             (org-agenda-files org-agenda-files)))
      (todo "READY"
            ((org-agenda-overriding-header "Ready for Work")
             (org-agenda-files org-agenda-files)))
      (todo "ACTIVE"
            ((org-agenda-overriding-header "Active Projects")
             (org-agenda-files org-agenda-files)))
      (todo "COMPLETED"
            ((org-agenda-overriding-header "Completed Projects")
             (org-agenda-files org-agenda-files)))
      (todo "CANC"
            ((org-agenda-overriding-header "Cancelled Projects")
             (org-agenda-files org-agenda-files)))))))

  ;; (setq org-capture-templates
  ;;   `(("t" "Tasks / Projects")
  ;;     ("tt" "Task" entry (file+olp "~/Projects/Code/emacs-from-scratch/OrgFiles/Tasks.org" "Inbox")
  ;;          "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)

  ;;     ("j" "Journal Entries")
  ;;     ("jj" "Journal" entry
  ;;          (file+olp+datetree "~/Projects/Code/emacs-from-scratch/OrgFiles/Journal.org")
  ;;          "\n* %<%I:%M %p> - Journal :journal:\n\n%?\n\n"
  ;;          ;; ,(dw/read-file-as-string "~/Notes/Templates/Daily.org")
  ;;          :clock-in :clock-resume
  ;;          :empty-lines 1)
  ;;     ("jm" "Meeting" entry
  ;;          (file+olp+datetree "~/Projects/Code/emacs-from-scratch/OrgFiles/Journal.org")
  ;;          "* %<%I:%M %p> - %a :meetings:\n\n%?\n\n"
  ;;          :clock-in :clock-resume
  ;;          :empty-lines 1)

  ;;     ("w" "Workflows")
  ;;     ("we" "Checking Email" entry (file+olp+datetree "~/Projects/Code/emacs-from-scratch/OrgFiles/Journal.org")
  ;;          "* Checking Email :email:\n\n%?" :clock-in :clock-resume :empty-lines 1)

  ;;     ("m" "Metrics Capture")
  ;;     ("mw" "Weight" table-line (file+headline "~/Projects/Code/emacs-from-scratch/OrgFiles/Metrics.org" "Weight")
  ;;      "| %U | %^{Weight} | %^{Notes} |" :kill-buffer t)))

  (setq org-src-fontify-natively t)

  :custom
  (with-eval-after-load 'org
    (setq org-format-latex-options (plist-put org-format-latex-options :scale 1.2))
    (setq org-format-latex-options (plist-put org-format-latex-options :foreground 'auto))
    (setq org-format-latex-options (plist-put org-format-latex-options :background 'auto)))
  )


(use-package evil-org
  :after org
  :config
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'org-mode-hook 'org-indent-mode)
  (add-hook 'evil-org-mode-hook
            (lambda ()
              (evil-org-set-key-theme)
              (local-set-key "\M-n" 'outline-next-visible-heading)
              (local-set-key "\M-p" 'outline-previous-visible-heading)
              ;; table
              (local-set-key "\C-\M-w" 'org-table-copy-region)
              (local-set-key "\C-\M-y" 'org-table-paste-rectangle)
              (local-set-key "\C-\M-l" 'org-table-sort-lines)
              ;; display images
              (local-set-key "\M-I" 'org-toggle-iimage-in-org)))
  (evil-leader/set-key-for-mode 'org-mode
     "oh" 'org-shiftleft
     "ol" 'org-shiftright
     "ok" 'org-shiftup
     "oj" 'org-shiftdown
     "osa" 'org-save-all-org-buffers
     "osc" 'org-schedule
     "odl" 'org-deadline
     "oto" 'org-todo
     "ott" 'org-show-todo-tree
     "ots" 'org-time-stamp
     "oit" 'org-insert-todo-heading
     "ome" 'org-mark-element
     "oag" 'org-agenda
     "oar" 'org-archive-subtree
     "ool" 'evil-org-open-links
     "orc" 'org-resolve-clocks))

(use-package org-bullets
  :after org
  :config
  (add-hook 'org-mode-hook
	    (lambda () (org-bullets-mode 1))))

(use-package org-journal
  :after org
  :defer t
  :config
  (setq org-journal-dir "~/journal/"
   	org-journal-date-format "#+TITLE: Journal Entry- %e %b %Y (%A)"
   	org-journal-time-format ""))

(use-package org-roam
  :after org
  :bind (:map org-mode-map
              ("C-M-i" . completion-at-point))
  :custom
  (org-roam-directory (file-truename "~/fikir/roam"))
  (org-roam-completion-everywhere t)
  :config
  (evil-leader/set-key-for-mode 'org-mode
     "onl" 'org-roam-buffer-toggle
     "onf" 'org-roam-node-find
     "ong" 'org-roam-graph
     "oni" 'org-roam-node-insert
     "onc" 'org-roam-capture
     "ondt" 'org-roam-dailies-capture-today
     )
  ;; If you're using a vertical completion framework, you might want a more informative completion interface
  (setq
   org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag))
   )
  ;; (add-hook 'completion-at-point-functions 'org-roam-completion-functions)
  (org-roam-db-autosync-mode))

(use-package calfw
  :commands (cfw:open-calendar-buffer))

(use-package calfw-org
  :commands (cfw:open-org-calendar
             cfw:org-create-source
             cfw:org-create-file-source
             cfw:open-org-calendar-withkevin
             my-open-calendar))

(use-package calfw-cal
  :commands (cfw:cal-create-source))

(use-package calfw-ical
  :commands (cfw:ical-create-source))

(provide 'pata-org-mode)
;;; pata-org-mode.el ends here
