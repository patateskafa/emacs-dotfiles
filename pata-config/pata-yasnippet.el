;;; pata-yasnippet.el --- YASnippet is a template system for Emacs. 
;;
;; Homepage: https://github.com/joaotavora/yasnippe
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package yasnippet
  :defer t
  :config
  (add-to-list 'yas-snippet-dirs (concat user-emacs-directory "pata-libs/pata-snippets"))
  (yas-global-mode 1)
  (yas-reload-all)
  (use-package helm-c-yasnippet
	  :bind (("C-c h y" . helm-yas-complete)
	  		("C-c c y" . company-yasnippet))))

(provide 'pata-yasnippet)
;;; pata-yasnippet.el ends here

