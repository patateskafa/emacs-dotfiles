;;; pata-markdown.el --- A major mode for editing Markdown-formatted text.
;;
;; Homepage: https://github.com/jrblevin/markdown-mode
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package markdown-mode
  :defer t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'"       . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :after org
  :init (setq markdown-command "/usr/bin/pandoc")
  :config
  (progn
    ;; http://endlessparentheses.com/ispell-and-org-mode.html
    (defun my/markdown-ispell ()
      "Configure `ispell-skip-region-alist' for `markdown-mode'."
      (make-local-variable 'ispell-skip-region-alist)
      (add-to-list 'ispell-skip-region-alist '("~" "~"))
      (add-to-list 'ispell-skip-region-alist '("```" "```")))
    (add-hook 'markdown-mode-hook 'my/markdown-ispell)

    ;; Markdown preferences
    (add-hook 'markdown-mode-hook
      (lambda ()
	    ;; Github markdown style
	    (setq markdown-css-paths `(,(expand-file-name "pata-libs/misc/github-pandoc.css" user-emacs-directory)))))))

(provide 'pata-markdown)
;;; pata-markdown.el ends here