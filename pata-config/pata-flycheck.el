;;; pata-flycheck.el --- On the fly syntax checking for GNU Emacs
;;
;; Homepage: https://github.com/flycheck/flycheck
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package flycheck
  :config
  (add-hook 'after-init-hook #'global-flycheck-mode)
  (global-flycheck-mode t)
  (flycheck-add-mode 'javascript-eslint 'web-mode)
  (setq flycheck-disabled-checkers
              (append flycheck-disabled-checkers
                      '(javascript-jshint json-jsonlist))
              ))

(provide 'pata-flycheck)
;;; pata-flycheck.el ends here
