;;; pata-ui.el --- TRAMP Mode configuration
;;
;; Homepage: https://www.gnu.org/software/tramp/
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;

(use-package tramp
  :defer t
  :bind (("C-c t" . helm-tramp))
  :config
  (setq tramp-default-method "ssh"
	  tramp-backup-directory-alist backup-directory-alist)
  (use-package helm-tramp
	  :config
	  (defalias 'exit-tramp 'tramp-cleanup-all-buffers)
	  ))

(provide 'pata-tramp)
;;; pata-tramp.el ends here