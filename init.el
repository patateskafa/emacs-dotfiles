;;; le petit init.el

(defconst emacs-start-time (current-time))

(set-language-environment "English")

;; update package-archive lists
(require 'package)

(setq package-enable-at-startup nil)

(add-to-list 'load-path (expand-file-name "pata-config" user-emacs-directory))
(add-to-list 'load-path (expand-file-name "pata-libs/lang" user-emacs-directory))

(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/"))
;;(add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/"))
;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

;; install 'use-package' if necessary
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(setq use-package-verbose t
      use-package-always-ensure t) ;; pass ensure t to all packages loaded by use-package

;; package list here
;; enable/disable any
(defconst pata-packages
  '(pata-better-defaults
    pata-general
    pata-ui
    pata-evil
    ;;pata-neotree
    pata-git
    pata-treemacs
    pata-helm
    pata-mode-line
    pata-smartparens
    pata-rainbow-delim
    pata-eyebrowse
    pata-org-mode
    pata-projectile
    pata-yasnippet
    pata-company
    pata-text-conf
    pata-w3m
    pata-flycheck
    pata-editorconfig
    ;;pata-prog-helper
    pata-yaml
    pata-tramp
    pata-eshell))

(defconst lang-packages
  '(pata-haskell
    pata-web
    pata-lsp
    pata-ccpp
    pata-clojure
    pata-elm))

(defconst gui-packages
  '(
    pata-gruvbox
    ;;pata-zenburn
    ;;pata-solarized
    pata-all-the-icons
    pata-pdf-tools
    pata-markdown
    pata-pandoc
    pata-reveal))

;; simple package loader
(defun load-ze-packages (plist)
  (dolist (ppack plist)
      (require ppack)
      (message "Loading %s..." ppack)))

;; load all ze packages!
(load-ze-packages pata-packages)
(load-ze-packages lang-packages)
(if (display-graphic-p)
    (load-ze-packages gui-packages)
  (load-theme 'wombat))

;;; post-initialization
(when window-system
  (let ((elapsed (float-time (time-subtract (current-time)
                                            emacs-start-time))))
    (message "Loading %s...done (%.3fs)" load-file-name elapsed))

  (add-hook 'after-init-hook
            `(lambda ()
               (let ((elapsed (float-time (time-subtract (current-time)
                                                         emacs-start-time))))
                 (message "Loading %s...done (%.3fs) [after-init]"
                          ,load-file-name elapsed)))
            t))
;;; init.el ends here
